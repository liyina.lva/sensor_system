# -*- coding: utf-8 -*-
from random import randrange

from observer_alarm_lights import ObserverAlarmLights
from observer_alarm_siren import ObserverAlarmSiren
from concrete_subject_sensor import ConcreteSubjectSensor


def _get_event():
    return randrange(100)


if __name__ == "__main__":
    observable = ConcreteSubjectSensor()

    alarm_lights = ObserverAlarmLights()
    observable.register(alarm_lights)
    alarm_siren = ObserverAlarmSiren()
    observable.register(alarm_siren)
    event = _get_event()
    observable.active_observers_alarm('Active alarm', event=event)
