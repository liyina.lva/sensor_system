# -*- coding: utf-8 -*-
from random import randrange

from subject_sensor import SubjectSensor


class ConcreteSubjectSensor(SubjectSensor):

    def __init__(self):
        self.observers = []

    def register(self, observer):
        if not observer in self.observers:
            self.observers.append(observer)

    def active_observers_alarm(self, *args, **kwargs):
        for observer in self.observers:
            observer.active(*args, **kwargs)
