# -*- coding: utf-8 -*-
from observer_alarm import Observer


class ObserverAlarmLights(Observer):

    def active(self, *args, **kwargs):
        if kwargs['event'] > 50:
            print("Observer alarm lights: {0}\n{1}".format(args, kwargs))
