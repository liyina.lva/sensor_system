# -*- coding: utf-8 -*-
from observer_alarm import Observer


class ObserverAlarmSiren(Observer):

    def active(self, *args, **kwargs):
        if kwargs['event'] > 80:
            say_bee = self.say_bee()
            print("Obsrever alarm siren: {0}\n{1} {2}".format(say_bee, args, kwargs))

    def say_bee(self):
        return "BEE"
