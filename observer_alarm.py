# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod


class Observer(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def active(self, *args, **kwargs):
        pass
